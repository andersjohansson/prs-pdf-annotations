;;; prs-pdf-annotations.el --- Extract and recreate pdf annotations for Sony PRS e-readers  -*- lexical-binding: t; -*-

;; Copyright (C) 2021-2022  Anders Johansson

;; Author: Anders Johansson <mejlaandersj@gmail.com>
;; Keywords: convenience
;; Created: 2021-05-31
;; Modified: 2022-02-25
;; Package-Requires: ((emacs "27.1") (pdf-tools "1.0.0") (pcsv "1.3.7"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'pdf-info)
(require 'pdf-isearch)
(require 'pdf-annot)
(require 'pcsv)
(require 'cl-lib)
(require 'subr-x)

(defgroup prs-pdf-annotations
  nil
  "Options for ‘prs-pdf-annotations’."
  :group 'external)

(defcustom prs-pdf-annotations-reader-path "~/"
  "The path to the reader’s mount-point.
On a modern linux system, something like “/run/media/user/READER/”."
  :type 'string)

(defcustom prs-pdf-annotations-import-directory "~/"
  "Directory for saving annotated pdf files from the unit."
  :type 'directory)

(defcustom prs-pdf-annotations-hl-color "#ffffcc"
  "Color for highlights made by the unit."
  :type 'string)

(defcustom prs-pdf-annotations-author "PRS"
  "Author name for annotations made by the unit."
  :type 'string)

(defun prs-pdf-annotations-fix-ligatures (searchstring)
  "Replace fi etc. with (fi|ﬁ) in SEARCHSTRING."
  (let ((lig '(("fi" . "ﬁ")
               ("ffi" . "ﬃ")
               ("ff" . "ﬀ")
               ("fl" . "ﬂ")
               ("ffl" . "ﬄ"))))
    (cl-loop for (f . r) in lig do
             (let ((ff (pdf-isearch-word-search-regexp f t pdf-isearch-hyphenation-character)))
               (setq searchstring
                     (string-replace ff (format "(?:%s|%s)" ff r) searchstring))))

    ;; This is due to some encoding bug in poppler (or pdfs), I believe a
    ;; non-breaking space or something is interpreted incorrectly in
    ;; some pdfs.
    (thread-last
        searchstring
      (string-replace "(?:[\\-­]\\n)" "(?:[\\-­]\\n|Â�)")
      (string-replace "\\W+" "(\\W|Â�)+"))))

;;;###autoload
(defun prs-pdf-annotations (&optional only-annotations)
  "Get annotations and pdf file from Sony PRS-TX unit.
Prefix argument ONLY-ANNOTATIONS only fetches annotations to an org buffer."
  (interactive "P")
  (let* ((dbfile (expand-file-name "Sony_Reader/database/books.db" prs-pdf-annotations-reader-path))
         (dbfile (if (file-readable-p dbfile)
                     dbfile
                   (read-file-name "Database file (books.db)" nil nil t)))
         (dburl (url-encode-url (concat "file://" dbfile "?mode=ro")))
         (callstring "sqlite3 -csv \"%s\" \"%s\"")
         books books2 annotations)
    (with-temp-buffer
      (call-process-shell-command
       (format callstring
               dburl
               "select _id, title, author, file_name, file_path, mime_type from books")
       nil t)
      (setq books (pcsv-parse-buffer)))
    (unless books
      (user-error "No books?"))
    (setq books2 (cl-loop for (id name author fn fp mt) in books
                          collect
                          (cons (concat name " — "
                                        author
                                        "	"
                                        (propertize (concat "(" fn ")") 'face 'shadow))
                                (list id fn fp mt))))
    (cl-destructuring-bind
        (bookid filename filepath mimetype)
        (cdr-safe (assoc (completing-read "Choose book: " books2) books2))
      (with-temp-buffer
        (call-process-shell-command
         (format callstring
                 dburl
                 (format
                  "select page, markup_type, name, marked_text from annotation where content_id = %s order by page"
                  bookid))
         nil t)
        (setq annotations (pcsv-parse-buffer)))
      (if annotations
          (if (and (not only-annotations) (string= "application/pdf" mimetype))
              (let ((newname (expand-file-name filename prs-pdf-annotations-import-directory))
                    (pdf-isearch-hyphenation-character "-­")
                    unmatched)
                (copy-file (expand-file-name filepath prs-pdf-annotations-reader-path) newname)
                (find-file newname)
                (print (length annotations))
                (cl-loop for (page type note marked) in annotations
                         do
                         (let ((pn (1+ (floor (string-to-number page))))
                               (contents (if (equal "11" type)
                                             note
                                           "")))
                           (if-let ((matched
                                     (pdf-info-search-regexp
                                      (prs-pdf-annotations-fix-ligatures
                                       (pdf-isearch-word-search-regexp marked t pdf-isearch-hyphenation-character))
                                      pn
                                      (current-buffer))))
                               (let* ((first (car matched)))
                                 (pdf-view-goto-page (alist-get 'page first))
                                 (pdf-annot-add-highlight-markup-annotation
                                  (alist-get 'edges first)
                                  "#ffffcc"
                                  `((label . ,prs-pdf-annotations-author)
                                    (contents . ,contents))))
                             (push (list page type note marked) unmatched))))
                (save-buffer)
                ;;debugging:
                (when unmatched
                  (message "Unmatched: %d" (length unmatched))
                  (pp-display-expression (nreverse unmatched) "unmatched")))

            ;; if not a pdf (probably epub) just extract the notes to an org-buffer
            (progn (switch-to-buffer (generate-new-buffer "*PRS annotations*"))
                   (cl-loop for (page type note marked) in annotations
                            do
                            (when (equal "11" type) ; if the note has text
                              (insert note ":\n"))
                            (insert "#+begin_quote\n"
                                    marked
                                    "\n"
                                    "(p. "
                                    (if (string-match "^\\([0-9]+\\)\\.0$" page)
                                        (match-string 1 page)
                                      page)
                                    ")\n"
                                    "#+end_quote\n\n"))
                   (goto-char (point-min))
                   (org-mode)))
        (message "No annotations found for that book")))))

(provide 'prs-pdf-annotations)
;;; prs-pdf-annotations.el ends here
